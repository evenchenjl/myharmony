## 学习日记（JS）

##### 2021年6月24日

1.harmony OS的容器默认是flex盒子

2.栅格布局，0< xs(2) < 320 <= sm(4) < 600 <= md(8) < 840 <= lg(12)

##### 2021年9月8日

1.console拼接参数不能用`,` 只能用`+`

## 未解决的问题

##### 2021年6月24日

1.媒体组件video，显示初始化播放器失败，无法播放视频

2.toolbar组件选中无法高亮状态

>toolbar-item CSS设置color无效

3.全局方法不知道怎么写

##### 2021年6月26日

1.list组件问题（猜测解决方案是计算出list的高度，同时scrollpage为false,还有注意底部是否有fixed元素）

> 不设置高度，高度是屏幕的高度，在页面上会有空白区域滚动，group展开也不会撑开高度,如果scrollpage是true,这时group展开的item会超出list,而且无法滚动查看

> 设置高度，高度固定，只能内部滚动，list的scrollpage属性不能设置为true,否则高度又会变成屏幕的高度

>**当list的item多到超过一个屏幕时，如果list后面还有内容，这时会滚动不下去，只能看到list的内容**

## 已解决的问题

##### 2021年6月24日

1.list组件的分割线样式要写在样式CSS里（已解决）

`.list{
      divider-color:#efefef;
  }
`

2.list-item被position元素遮挡时，给list的父级元素只设置padding无效，list的***scrollpage***也要为true才行（已解决）

```
//html
<div class="container">
    <text class="title">
        基础组件
    </text>
    <list class="list" divider="true" scrollpage="true">//scrollpage关键
        <list-item type="listItem" for="{{list}}" class="list-item" on:click="goPage({{$item.path}})">
            <text class="desc-text">{{$item.name}}</text>
            <image src="common/images/right-arrow.png" class="desc-img"></image>
        </list-item>
    </list>
    <mytabbar></mytabbar>
</div>
//css
.container{
    padding-bottom: 56px;
}
```

3.栅格组件的grid-row里的grid-col不会自动换行，加上下面的CSS就可以
 
 `.grid-row{
      flex-wrap: wrap;
      justify-content: space-between;
  }`
  
  `justify-content: space-between;`这个可以让grid-col两端对齐,这个可以根据实际情况改变