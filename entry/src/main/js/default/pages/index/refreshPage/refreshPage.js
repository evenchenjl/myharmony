import prompt from '@system.prompt';
export default {
    data(){
        return{
            refreshingVal:false
        }
    },
    pulldownEvent(e){
        console.log("下拉临界值"+e.state)
        prompt.showToast({
            message: '下拉临界值' + e.state,
            duration: 3000,
        });
    },
    refreshEvent(e){
        this.refreshingVal = e.refreshing;//把刷新状态标记为正在刷新
        setTimeout(() => {
            this.refreshingVal = false;//获取数据后把刷新状态变为结束
            prompt.showToast({
                message: "close refresh"
            })
        }, 3000)
    }
}
