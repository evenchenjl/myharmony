import router from '@system.router';
import goPage from '../../common/utils.js'
export default {
    data: {
        list:[
            { name:'button(按钮)', path:'/buttonPage/buttonPage' },
            { name:'input(交互式组件)', path:'/inputPage/inputPage' },
            { name:'textarea(文本域)', path:'/textareaPage/textareaPage' },
            { name:'label(标注)', path:'/labelPage/labelPage' },
            { name:'chart(数据图形化)', path:'/chartPage/chartPage' },
            { name:'divider(分隔器)', path:'/dividerPage/dividerPage' },
            { name:'image(图片)', path:'/imagePage/imagePage' },
            { name:'marquee(跑马灯)', path:'/marqueePage/marqueePage' },
            { name:'menu(菜单)', path:'/menuPage/menuPage' },
            { name:'select(下拉选择)', path:'/selectPage/selectPage' },
            { name:'picker(滑动选择器)', path:'/pickerPage/pickerPage' },
            { name:'pickerview(嵌入页面的滑动选择器)', path:'/pickerviewPage/pickerviewPage' },
            { name:'piece(块状入口)', path:'/piecePage/piecePage' },
            { name:'progress(进度条)', path:'/progressPage/progressPage' },
            { name:'qrcode(二维码)', path:'/qrcodePage/qrcodePage' },
            { name:'rating(评分条)', path:'/ratingPage/ratingPage' },
            { name:'richtext(富文本)', path:'/richtextPage/richtextPage' },
            { name:'search(搜索框)', path:'/searchPage/searchPage' },
            { name:'slider(滑动条)', path:'/sliderPage/sliderPage' },
            { name:'switch(开关选择器)', path:'/switchPage/switchPage' },
            { name:'toolbar(底部导航)', path:'/toolbarPage/toolbarPage' },
            { name:'toggle(状态按钮)', path:'/togglePage/togglePage' },
            { name:'web(外链)', path:'/webPage/webPage' },
        ]
    },
    onInit() {
    },
    goPage(path) {
        console.log('页面路径pages/basics' + path)
        router.push({
            uri: 'pages/basics' + path
        })
    }
}
