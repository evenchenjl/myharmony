import prompt from '@system.prompt'
export default {
    data:{
        return(){
        }
    },
    change(e){
        prompt.showToast({
            message: "value: " + e.value,
            duration: 3000,
        });
    },
    enteredClick(e){
        prompt.showToast({
            message: "enterkey clicked",
            duration: 3000,
        });
    },
    buttonClick(e){
        this.$element("input").showError({
            error: 'error text'
        });
    },
    checkboxOnChange(value,e) {
        prompt.showToast({
            message: value +': '+ e.checked,
            duration: 3000,
        });
    },
    onRadioChange(inputValue, e) {
        console.log(e.value)
        if (inputValue === e.value) {
            prompt.showToast({
                message: 'The chosen radio is ' + e.value,
                duration: 3000,
            });
        }
    }
}
