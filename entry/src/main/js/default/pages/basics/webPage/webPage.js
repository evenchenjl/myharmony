import prompt from '@system.prompt';
export default {
    data: {
        title: 'World'
    },
    onFinish(e){
        prompt.showToast({
            message: '网页加载完成'+e.url
        });
    },
    onStart(e){
        prompt.showToast({
            message: '网页开始加载'+e.url
        });
    },
    onError(e){
        prompt.showToast({
            message: '网页加载错误'+e.description
        });
    }
}
