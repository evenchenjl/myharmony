export default {
    data: {
        fit:"cover",
        fits: ["cover", "contain", "fill", "none", "scale-down"],
        frames: [
            { src: "/common/images/detail1.jpg",},
            { src: "/common/images/detail2.jpg",},
            { src: "/common/images/list.jpg",},
            { src: "/common/images/swiper1.png",},
            { src: "/common/images/swiper2.png",},
            { src: "/common/images/swiper3.png",},
            { src: "/common/images/ylj0.jpg",},
            { src: "/common/images/ylj00.jpg",},
            { src: "/common/images/ylj1.jpg",},
            { src: "/common/images/ylj2.jpg",},
            { src: "/common/images/ylj3.jpg",},
        ],
    },
    change_fit(e) {
        this.fit = e.newValue;
    },
    handleStart() {
        this.$refs.animator.start();
    },
    handlePause() {
        this.$refs.animator.pause();
    },
    handleResume() {
        this.$refs.animator.resume();
    },
    handleStop() {
        this.$refs.animator.stop();
    },
}
