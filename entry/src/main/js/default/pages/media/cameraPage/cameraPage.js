export default {
    data(){
        return{
            cameraList:[
                { title: '属性',list:['flash(闪关灯)，string，默认off，取值为on、off、torch（手电筒常亮模式）',
                    'deviceposition(前置后置)，string，默认back，取值为front、back。']},
                { title: '事件',list:['error，用户不允许使用摄像头时触发。']},
                { title: '方法',list:['takePhoto，执行拍照，支持设置图片质量。参数：CameraTakePhotoOptions',
                    'CameraTakePhotoOptions：quality，string，必填，默认normal，图片质量，可能值有：high、normal、low；success（返回图片的uri）；fail；complete',],
                }
            ]
        }
    }
}
