// xxx.js
export default {
    data: {
        modeFlag: "half"
    },
    showPanel(e) {
        this.$element('simplepanel').show()
    },
    closePanel(e) {
        this.$element('simplepanel').close()
    },
    changeMode(e) {
        this.modeFlag = e.mode
    }
}
