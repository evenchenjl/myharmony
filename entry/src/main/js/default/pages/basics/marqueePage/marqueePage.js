export default {
    data: {
        scrollAmount: 6,
        loop: 3,
        marqueeDir: 'left',
        marqueeCustomData: '跑马灯文字跑马灯文字跑马灯文字',
    },
    onMarqueeBounce: function() {
        console.log("onMarqueeBounce");
    },
    onMarqueeStart: function() {
        console.log("onMarqueeStart");
    },
    onMarqueeFinish: function() {
        console.log("onMarqueeFinish");
    },
    onStartClick (evt) {
        this.$element('customMarquee').start();
    },
    onStopClick (evt) {
        this.$element('customMarquee').stop();
    }
}