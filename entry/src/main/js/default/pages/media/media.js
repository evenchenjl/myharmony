import router from '@system.router';
export default {
    data(){
        return{
            list:[
                { name:'video(视频)', path:'/videoPage/videoPage' },
                { name:'camera(相机)', path:'/cameraPage/cameraPage' },
            ]
        }
    },
    onInit() {
    },
    goPage(path){
        router.push({
            uri: 'pages/media'+ path
        })
    }
}