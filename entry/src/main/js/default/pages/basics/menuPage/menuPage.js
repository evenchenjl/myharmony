import prompt from '@system.prompt';
export default {
    onMenuSelected(e) {
        prompt.showToast({
            message: e.value
        })
    },
    onMenuCancel(){
        prompt.showToast({
            message:'用户取消了'
        })
    },
    onTextClick(id) {
        if(id== 'apiMenu'){
            this.$element(id).show({x:280,y:80});
        }else{
            this.$element(id).show({x:280,y:300});
        }

    }
}
