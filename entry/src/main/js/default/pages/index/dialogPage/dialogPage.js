// xxx.js
import prompt from '@system.prompt';

export default {
    showDialog(e) {
        this.$element('simpledialog').show()
    },
    cancelDialog(e) {
        prompt.showToast({
            message: 'Dialog 取消'
        })
    },
    cancelSchedule(e) {
        this.$element('simpledialog').close()
        prompt.showToast({
            message: '成功取消'
        })
    },
    setSchedule(e) {
        this.$element('simpledialog').close()
        prompt.showToast({
            message: '成功确定'
        })
    }
}