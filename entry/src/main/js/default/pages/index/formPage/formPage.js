export default {
    data: {
        title: 'World'
    },
    onSubmit(result) {
        console.log(result.value.radioGroup) // radio1 or radio2
        console.log(result.value.user) // text input value
    },
    onReset() {
        console.log('reset all value')
    }
}
