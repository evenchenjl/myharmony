import router from '@system.router';
export default {
    props: ['currentIndex'],
    data() {
        return {
            list:[
                { name:'容器组件', path:'pages/index/index', icon:'common/Icon/heart.png' },
                { name:'基础组件', path:'pages/basics/basics', icon:'common/Icon/heart.png' },
                { name:'媒体组件', path:'pages/media/media', icon:'common/Icon/heart.png' },
                { name:'画布组件', path:'pages/canvasPage/canvasPage', icon:'common/Icon/heart.png' },
                { name:'栅格组件', path:'pages/grid/grid', icon:'common/Icon/heart.png' },
            ],
            index: this.currentIndex,
            fontColor: '#ef2f2f',
        }
    },
    onInit(){

    },
    onShow(){

    },
    goToolbar(path){
        let uri = path
        let currentPage = router.getState()
        let currentPath = currentPage.path + currentPage.name
        if(currentPath == uri){
            return false
        }else{
            router.replace({
                uri: path
            })
        }
    }
}
