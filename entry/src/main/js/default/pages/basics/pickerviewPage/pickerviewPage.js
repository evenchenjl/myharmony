export default {
    data: {
        defaultTime: "2020-06-06-11-02",
        time: ""
    },
    onInit() {
//        this.defaultTime = this.now();
    },
    handleChange(e) {
        this.time = e.year+"-"+e.month+"-"+e.day+" "+e.hour+":"+e.minute;
    },
    now() {
        const date = new Date();
        const hours = date.getHours();
        const minutes = date.getMinutes();
        return this.concat(hours, minutes);
    },

    fill(value) {
        return (value > 9 ? "" : "0") + value;
    },

    concat(hours, minutes) {
        return `${this.fill(hours)}:${this.fill(minutes)}`;
    },
}
