export default {
    data: {
        list:[
            { name:'item1' },
            { name:'item2' },
            { name:'divider分割线', group:[ 'divider分割线(CSS)', 'divider-color', 'divider-height', 'divider-origin', 'divider-length', ] },
            { name:'item4' },
            { name:'item5', group:[ 'list-item','primary', 'group-item2' ]  },
            { name:'item6' },
        ],
        listDesc:[
            {
                title:'list',
                desc:[
                    {
                        value:'scrollpage(Boolean)false',
                        desc:'设置为true时，将 list 顶部页面中非 list 部分随 list 一起滑出可视区域，当list方向为row时，不支持此属性。'
                    },
                    {
                        value: 'cachedcount(Number)0',
                        desc: '长列表延迟加载时list-item最少缓存数量。可视区域外缓存的list-item数量少于该值时，会触发requestitem事件。'
                    },
                    {
                        value: 'scrollbar(String)off',
                        desc: '侧边滑动栏的显示模式（当前只支持纵向）：off：不显示。auto：按需显示(触摸时显示，2s后消失)。on：常驻显示。'
                    },
                    {
                        value: 'scrolleffect(String)spring',
                        desc: '滑动效果，spring：弹性物理动效。fade：渐隐物理动效。no：滑动到边缘后无效果。'
                    },
                    {
                        value: 'indexer(boolean | Array<string>)false',
                        desc: 'true:表示使用默认字母索引表。false:表示无索引。["#", "A", "Y", "Z"]表示自定义索引表。自定义时"#"必须要存在。',
                        sm:'说明：1.indexer属性生效需要flex-direction属性配合设置为column，且columns属性设置为1。2.点击索引条进行列表项索引需要list-item子组件配合设置相应的section属性。'
                    },
                    {
                        value: 'indexercircle(Boolean)',
                        desc: '是否为环形索引。穿戴设备默认为true，其他为false。indexer为false时不生效。'
                    },
                    {
                        value: 'indexermulti(Boolean)false',
                        desc: '是否开启索引条多语言功能。indexer为false时不生效。'
                    },
                    {
                        value: 'indexerbubble(Boolean)true',
                        desc: '是否开启索引切换的气泡提示。indexer为false时不生效。'
                    },
                    {
                        value: 'divider(Boolean)false',
                        desc: 'item是否自带分隔线。其样式参考样式列表的divider-color、divider-height、divider-length、divider-origin。'
                    },
                    {
                        value: 'shapemode(String)defalut',
                        desc: '侧边滑动栏的形状类型。default：不指定，跟随主题；rect：矩形；round：圆形。'
                    },
                    {
                        value: 'itemscale(Boolean)true',
                        desc: '焦点处理时，可以通过这个属性取消焦点进出的放大缩小效果。该效果仅在智能穿戴和智慧屏上生效。',
                        sm: '说明:仅在columns样式为1的时候生效。'
                    },
                    {
                        value: 'itemcenter(Boolean)false',
                        desc: '初始化页面和滑动后页面总是有一个item处于视口交叉轴的中心位置。该效果仅在智能穿戴上生效。'
                    },
                    {
                        value: 'updateeffect(Boolean)false',
                        desc: '用于设置当list内部的item发生删除或新增时是否支持动效。false：新增删除item时无过渡动效。true：新增删除item时播放过程动效。'
                    },
                    {
                        value: 'chainanimation(Boolean)false',
                        desc: '用于设置当前list是否启用链式联动动效，开启后列表滑动以及顶部和底部拖拽时会有链式联动的效果。链式联动效果：list内的list-item间隔一定距离，在基本的滑动交互行为下，主动对象驱动从动对象进行联动，驱动效果遵循弹簧物理动效。false:不启用链式联动true：启用链式联动',
                        sm: '说明:1.不支持动态修改。2.如同时配置了indexer，链式动效不生效。3.如配置了链式动效，list-item的sticky不生效。'
                    },
                    {
                        value: 'scrollvibrate(Boolean)true',
                        desc: '用于设置当list滑动时是否有振动效果，仅在智能穿戴场景生效，其他场景滑动无振动效果。false：列表滑动时有振动效果。true：列表滑动时无振动效果'
                    },
                    {
                        value: 'initialindex(Number)0',
                        desc: '用于设置当前List初次加载时视口起始位置显示的item，默认为0，即显示第一个item，如设置的序号超过了最后一个item的序号，则设置不生效，当同时设置了initialoffset属性时，当前属性不生效。当indexer为true或者scrollpage为true时，不生效。'
                    },
                    {
                        value: 'initialoffset(length)0',
                        desc: '用于设置当前List初次加载时视口的起始偏移量，偏移量无法超过当前List可滑动的范围，如果超过会被截断为可滑动范围的极限值。当indexer为true或者scrollpage为true时，不生效。'
                    },
                    {
                        value: 'selected(string)-',
                        desc: '指定当前列表中被选中激活的项，可选值为list-item的section属性值。'
                    },
                ]
            },
            { title:'list-item' },
            { title:'list-item-group' },
        ]
    }
}
