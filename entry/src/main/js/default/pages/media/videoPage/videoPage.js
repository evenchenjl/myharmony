export default {
    data() {
        return {
            event: '',
            seekingtime: '',
            timeupdatetime: '',
            seekedtime: '',
            isStart: true,
            isfullscreenchange: false,
            duration: '',
        }
    },
    preparedCallback: function (e) {
        this.event = '视频连接成功';
        this.duration = e.duration;
    },
    startCallback: function () {
        this.event = '视频开始播放';
    },
    pauseCallback: function () {
        this.event = '视频暂停播放';
    },
    finishCallback: function () {
        this.event = '视频播放结束';
    },
    errorCallback: function () {
        this.event = '视频播放错误';
    },
    seekingCallback: function (e) {
        this.seekingtime = e.currenttime;
    },
    timeupdateCallback: function (e) {
        this.timeupdatetime = e.currenttime;
    },
    change_start_pause: function () {
        if (this.isStart) {
            this.$element('myVideo').pause();
            this.isStart = false;
        } else {
            this.$element('myVideo').start();
            this.isStart = true;
        }
    },
    change_fullscreenchange: function () { //全屏
        if (!this.isfullscreenchange) {
            this.$element('myVideo').requestFullscreen({
                screenOrientation: 'default'
            });
            this.isfullscreenchange = true;
        } else {
            this.$element('myVideo').exitFullscreen();
            this.isfullscreenchange = false;
        }
    }
}