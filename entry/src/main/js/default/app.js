import common from './common/utils.js'

export default {
    data: {
        return() {
            appData:'我是全局变量'
        }
    },
    globalData: {
        appData: 'appData',
        appVersion: '2.0',
    },
    globalMethod() {
        console.info('This is a global method!');
//        this.globalData.appVersion = '3.0';
    },
    onCreate() {
        console.info('AceApplication onCreate');
    },
    onDestroy() {
        console.info('AceApplication onDestroy');
    },
    goPage1(path) {
        common.goPage(path)
    },
    test(e) {
        console.log('全局方法' + e)
    }
};
