import router from '@system.router';
import common1 from '../../common/utils.js'
export default {
    data(){
        return{
            list:[
                { name:'badge(角标)', path:'/badgePage/badgePage' },
                { name:'dialog(弹窗)', path:'/dialogPage/dialogPage' },
                { name:'list(列表)', path:'/listPage/listPage' },
                { name:'panel(可滑动面板)', path:'/panelPage/panelPage' },
                { name:'popup(气泡提示)', path:'/popupPage/popupPage' },
                { name:'refresh(下拉刷新)', path:'/refreshPage/refreshPage' },
                { name:'stack(堆叠容器)', path:'/stackPage/stackPage' },
                { name:'stepper(步骤导航)', path:'/stepperPage/stepperPage' },
                { name:'swiper(轮播)', path:'/swiperPage/swiperPage' },
                { name:'tabs(tab切换)', path:'/tabsPage/tabsPage' },
                { name:'form(表单)', path:'/formPage/formPage' },
            ],
            appData: 'appData1',
            appVersion: '2.1',
        }
    },
    onInit() {
        //调用app.js的方法
        this.$app.test('aaa')
        //调用app.js自定义全局方法
        this.$app.goPage1('bbb')
        // 局部调用自定义方法 传参无效
        common1.goPage('sss')
        this.invokeGlobalMethod()
        this.getAppVersion()
        console.log(this.$app.$def.globalData.appData+this.$app.$def.globalData.appVersion)
    },
    goPage(path){
        router.push({
            uri: 'pages/index'+ path
        })
    },
    invokeGlobalMethod() {
        this.$app.$def.globalMethod();
    },
    getAppVersion() {
        console.log(this.appVersion)
        this.appVersion = this.$app.$def.globalData.appVersion;
        console.log(this.appVersion)
    }
}
